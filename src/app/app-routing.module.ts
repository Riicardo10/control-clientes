import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PanelComponent } from './components/panel/panel.component';
import { LoginComponent } from './components/login/login.component';
import { RegistroComponent } from './components/registro/registro.component';
import { ConfiguracionComponent } from './components/configuracion/configuracion.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { AuthGuard } from './_guards/auth.guard';
import { ConfigGuard } from './_guards/config.guard';


const routes: Routes = [
  { path: '', component: PanelComponent, canActivate: [AuthGuard] },
  { path: 'login', component: LoginComponent },
  { path: 'configuracion', component: ConfiguracionComponent, canActivate: [AuthGuard] },
  { path: 'registro', component: RegistroComponent, canActivate: [ConfigGuard] },
  { path: '**', component: NotFoundComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

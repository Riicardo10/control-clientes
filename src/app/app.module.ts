import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { environment } from '../environments/environment';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule, FirestoreSettingsToken } from '@angular/fire/firestore';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { FlashMessagesModule } from 'angular2-flash-messages';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { PanelComponent } from './components/panel/panel.component';
import { LoginComponent } from './components/login/login.component';
import { ConfiguracionComponent } from './components/configuracion/configuracion.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { FooterComponent } from './components/footer/footer.component';
import { RegistroComponent } from './components/registro/registro.component';
import { ClientesComponent } from './components/clientes/clientes.component';
import { ClienteEditarComponent } from './components/cliente-editar/cliente-editar.component';
import { LoginService } from './_services/login.service';
import { ClienteService } from './_services/cliente.service';
import { AuthGuard } from './_guards/auth.guard';
import { RegisterService } from './_services/register.service';
import { ConfigService } from './_services/config.service';
import { ConfigGuard } from './_guards/config.guard';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    PanelComponent,
    LoginComponent,
    ConfiguracionComponent,
    NotFoundComponent,
    FooterComponent,
    RegistroComponent,
    ClientesComponent,
    ClienteEditarComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularFireModule.initializeApp( environment.firestore, 'control-clientes' ),
    AngularFirestoreModule,
    AngularFireAuthModule,
    FormsModule,
    FlashMessagesModule.forRoot()
  ],
  providers: [
    LoginService,
    ClienteService,
    AuthGuard,
    ConfigGuard,
    RegisterService,
    ConfigService,
    {
      provide: FirestoreSettingsToken, 
      useValue: {}
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

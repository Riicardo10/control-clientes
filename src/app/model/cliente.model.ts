export interface Cliente {
    id?:        string;
    nombre?:    string;
    apellidos?: string;
    email?:     string;
    saldo?:     number;
}
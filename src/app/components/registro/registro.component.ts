import { Component, OnInit } from '@angular/core';
import { FlashMessagesService } from 'angular2-flash-messages';
import { LoginService } from '../../_services/login.service';
import { Router } from '@angular/router';
import { RegisterService } from 'src/app/_services/register.service';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent implements OnInit {

  public email: string;
  public password: string;
  constructor(
    private router: Router,
    private flashMessages: FlashMessagesService,
    private serviceLogin: LoginService,
    private serviceRegister: RegisterService,
  ) { }

  ngOnInit() {
  }
  
  registrar() {
    this.serviceRegister.registro( this.email, this.password  )
      .then( res => {
        this.router.navigate( [ '' ] )
      } )
      .catch( err => {
        this.flashMessages.show( err, {
          cssClass: 'alert-danger',
          timeout: 4000
        } )
      } )
  }

}

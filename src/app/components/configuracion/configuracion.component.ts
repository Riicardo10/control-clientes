import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ConfigService } from '../../_services/config.service';
import { Config } from '../../model/config.model';

@Component({
  selector: 'app-configuracion',
  templateUrl: './configuracion.component.html',
  styleUrls: ['./configuracion.component.css']
})
export class ConfiguracionComponent implements OnInit {

  public permitirRegistro: boolean = false;

  constructor(
    private router: Router,
    private serviceConfig: ConfigService
  ) { }

  ngOnInit() {
    this.serviceConfig.getConfig()
      .subscribe( (res: Config) => {
        this.permitirRegistro = res.permitirRegistro;
      } )
  }

  guardar() {
    let config = {
      permitirRegistro: this.permitirRegistro
    }
    this.serviceConfig.modificarConfig( config );
    this.router.navigate( [ '/' ] )
  }

}

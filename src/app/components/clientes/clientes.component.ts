import { Component, OnInit } from '@angular/core';
import { ClienteService } from './../../_services/cliente.service';
import { Cliente } from './../../model/cliente.model';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Alert } from 'selenium-webdriver';

@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.component.html',
  styleUrls: ['./clientes.component.css']
})

export class ClientesComponent implements OnInit {

  public clienteIdEliminar:string;
  public accion: number = 0;
  public clientes: Cliente[];
  public cliente: Cliente = {
    nombre: '',
    apellidos: '',
    email: '',
    saldo: 0
  }

  constructor(
    private serviceCliente: ClienteService,
    private flashMessages: FlashMessagesService
  ) { }

  ngOnInit() {
    this.serviceCliente.getClientes().subscribe( res => {
      this.clientes = res;
    } )
  }

  getSaldoTotal() {
    let total:number = 0;
    if( this.clientes ) {
      this.clientes.forEach( item => {
        total += item.saldo;
      }  )
    }
    return total;
  }

  onSubmitAgregarCliente( {value, valid}: {value:Cliente, valid:boolean} ) {
    if(!valid) {
      this.flashMessages.show( 'Llena el formulario correctamente', {
        cssClass: 'alert-danger',
        timeout: 3000
      } );
    }
    else{
      if( this.accion == 1 ) {
        this.serviceCliente.addCliente( value );
      }
      else if( this.accion == 2 ) {
        value.id = this.cliente.id;
        this.serviceCliente.updateCliente( value )
      }
      this.resetearObjetoCliente();
    }
  }
  
  accionAgregarCliente() {
    this.accion = 1;
    this.resetearObjetoCliente();
  }
  accionEditarCliente( cliente:Cliente ) {
    this.accion = 2;
    this.cliente.id = cliente.id;
    this.cliente.nombre = cliente.nombre;
    this.cliente.apellidos = cliente.apellidos;
    this.cliente.email = cliente.email;
    this.cliente.saldo = cliente.saldo;
  }

  eliminarCliente() {
    if (confirm( 'Seguro que desea eliminar este registro?' )) {
      this.serviceCliente.deleteCliente( this.cliente.id );
      document.getElementById( 'clienteModal' ).style.visibility = 'none';
    }
  }

  resetearObjetoCliente() {
    this.cliente.id = '';
    this.cliente.nombre = '';
    this.cliente.apellidos = '';
    this.cliente.saldo = 0;
    this.cliente.email = '';
  }

}

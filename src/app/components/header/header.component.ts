import { Component, OnInit } from '@angular/core';
import { LoginService } from '../../_services/login.service';
import { Router } from '@angular/router';
import { ConfigService } from '../../_services/config.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  public isLogged: boolean;
  public userLogged: string;
  public currentUrl: string;
  public permitirRegistro: boolean;

  constructor(
    private serviceLogin: LoginService,
    private router: Router,
    private serviceConfig: ConfigService
  ) {
    this.currentUrl =  window.location.pathname;
  }

  ngOnInit() {
    this.serviceLogin.getAuth()
      .subscribe(res => {
        if (res) {
          this.isLogged = true;
          this.userLogged = res.email;
        }
        else {
          this.isLogged = false;
        }
      })
    this.serviceConfig.getConfig()
      .subscribe( res => {
        this.permitirRegistro = res.permitirRegistro;
      } )
  }

  ngAfterContentChecked(): void {
    this.currentUrl =  window.location.pathname;
  }

  logout() {
    this.serviceLogin.logout();
    this.isLogged = false;
    this.router.navigate(['/login'])
  }

}

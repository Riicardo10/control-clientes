import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';
import { LoginService } from 'src/app/_services/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public email: string;
  public password: string;

  constructor(
    private router: Router,
    private flashMessages: FlashMessagesService,
    private serviceLogin: LoginService
  ) { }

  ngOnInit() {
    this.serviceLogin.getAuth()
      .subscribe( res => {
        this.router.navigate( ['/'] )
      } )
  }

  login() {
    this.serviceLogin.login( this.email, this.password )
      .then( res => {
        this.router.navigate( ['/'] )
      } )
      .catch( err => {
        this.flashMessages.show( err.message, {
          cssClass: 'alert-danger',
          timeout: 4000
        } )
      } )
      
  }

}

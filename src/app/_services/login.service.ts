import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { map } from 'rxjs/operators';

@Injectable()
export class LoginService {

  constructor(
    private authService: AngularFireAuth
  ) { }

  login(email:string, password:string) {
    return new Promise((resolve, reject) => {
      this.authService.auth.signInWithEmailAndPassword( email, password )
        .then( res => {
          return resolve( res );
        }, err => {
          return reject( err )
        } )
    })
  }

  getAuth() {
    return this.authService.authState.pipe(
      map( auth => {
        return auth;
      } )
    )
  }

  logout() {
    this.authService.auth.signOut();  
  }

}

import { Injectable } from '@angular/core';
import { AngularFirestoreDocument, AngularFirestore } from '@angular/fire/firestore';
import { Config } from '../model/config.model';
import { Observable } from 'rxjs';

@Injectable()
export class ConfigService {

  public configDocument:    AngularFirestoreDocument<Config>;
  public config:            Observable<Config>;

  private id = '1';

  constructor(
    private db: AngularFirestore
  ) { }

  getConfig(): Observable<Config> {
    this.configDocument = this.db.doc<Config>( `configuracion/${this.id}` );
    this.config = this.configDocument.valueChanges();
    return this.config;
  }

  modificarConfig( config: Config ) {
    this.configDocument = this.db.doc<Config>( `configuracion/${this.id}` );
    this.configDocument.update( config );
  }

}

import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';

@Injectable()
export class RegisterService {

  constructor(
    private authService: AngularFireAuth
  ) { }

  registro( email:string, password:string ) {
    return new Promise( (resolve, reject) => {
      this.authService.auth.createUserWithEmailAndPassword( email, password )
        .then( res => {
          return resolve( res )
        } )
        .catch( err => {
          return reject( err )
        } )
    } )
  }

}

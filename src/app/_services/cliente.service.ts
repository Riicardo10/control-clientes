import { Injectable } from '@angular/core';
import { AngularFirestoreCollection, AngularFirestoreDocument, AngularFirestore } from '@angular/fire/firestore';
import { Cliente } from '../model/cliente.model';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class ClienteService {

  clientesCollection: AngularFirestoreCollection<Cliente>;
  clienteDocument:    AngularFirestoreDocument<Cliente>;
  clientes:           Observable<Cliente[]>;
  cliente:            Observable<Cliente>;

  constructor( private db: AngularFirestore ) { 
    this.clientesCollection = db.collection( 'clientes', ref => ref.orderBy( 'nombre', 'asc' ) );
  }

  addCliente( cliente:Cliente ) {
    return this.clientesCollection.add(cliente);
  }

  getClientes(): Observable<Cliente[]> {
    this.clientes = this.clientesCollection.snapshotChanges().pipe( 
      map( cambios => {
        return cambios.map( accion => {
          const datos = accion.payload.doc.data() as Cliente;
          datos.id    = accion.payload.doc.id;
          return datos;
        } )
      } )
    )
    return this.clientes;
  }

  // getCliente(id:string) {
  //   this.clienteDocument = this.db.doc<Cliente>( `clientes/${id}` );
  //   this.cliente = this.clienteDocument.snapshotChanges()
  //     .pipe(
  //       map( res => {
  //         if( res.payload.exists === false ) {
  //           return null;
  //         }
  //         else {
  //           datos.id = res.payload.id;
  //           return datos;
  //         }
  //       } )
  //     )
  //   return this.cliente;
  // }

  updateCliente( cliente:Cliente ) {
    this.clienteDocument = this.db.doc( `clientes/${cliente.id}` );
    this.clienteDocument.update( cliente );
  }

  deleteCliente(id:string) {
    this.clienteDocument = this.db.doc( `clientes/${id}` );
    this.clienteDocument.delete( );
  }

}

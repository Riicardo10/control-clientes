import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { ConfigService } from '../_services/config.service';
import { map } from 'rxjs/operators';

@Injectable()
export class ConfigGuard implements  CanActivate {
  
  constructor(
    private router: Router,
    private serviceConfig: ConfigService
  ) {}

  canActivate(): Observable<boolean> {
    return this.serviceConfig.getConfig().pipe(
      map( res => {
        console.log(res)
        if(res.permitirRegistro) {
          return true;  
        }
        else{
          this.router.navigate( [ '/login' ] );
          return false;
        }
      } )
    )
  }

}
